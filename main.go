package main

import (
	"bufio"
	"database/sql"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"

	_ "github.com/mattn/go-sqlite3"
)

func initDB(dbName string) (*sql.DB, error) {
	dbExists := true
	if _, err := os.Stat(dbName); os.IsNotExist(err) {
		dbExists = false
	}

	db, err := sql.Open("sqlite3", dbName)
	if err != nil {
		return nil, err
	}

	if !dbExists {
		query := `
		CREATE TABLE IF NOT EXISTS url_records (
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		url TEXT NOT NULL
		);
		`

		_, err := db.Exec(query)
		if err != nil {
			log.Fatal("Failed to create table " + err.Error())
		}
	}

	return db, nil
}

func readOptionInput(s *bufio.Scanner) string {
	re := regexp.MustCompile(`^(\d+|n)$`)

	for {
		s.Scan()
		txt := s.Text()

		if re.MatchString(txt) {
			return txt
		}
		fmt.Println("Invalid input. Must be a number or n: ")
	}

}

func readURLInput(scanner *bufio.Scanner) string {
	fmt.Println("Enter the URL: ")
	if scanner.Scan() {
		txt := scanner.Text()
		sub := txt[0:3]
		if sub != "http" {
			txt = "http://" + txt
		}
		fmt.Println(txt)
		return txt
	}

	if err := scanner.Err(); err != nil {
		return ""
	}
	return ""
}

func listEntries(db *sql.DB) (int, error) {
	rows, err := db.Query("SELECT id, url FROM url_records")
	if err != nil {
		return 0, err
	}
	defer rows.Close()

	count := 0
	for rows.Next() {
		var id int
		var url string
		if err := rows.Scan(&id, &url); err != nil {
			return 0, err
		}
		fmt.Printf("ID: %d, URL: %s\n", id, url)
		count++
	}

	if err = rows.Err(); err != nil {
		return 0, err
	}

	return count, nil
}

func getURLById(db *sql.DB, id int) (string, error) {
	var url string
	query := "SELECT url FROM url_records WHERE id = ?"
	err := db.QueryRow(query, id).Scan(&url)
	if err != nil {
		if err == sql.ErrNoRows {
			return "", fmt.Errorf("no record with id %d", id)
		}
	}

	return url, nil
}

func addURL(db *sql.DB, url string) error {
	query := "INSERT INTO url_records (url) VALUES (?)"
	result, err := db.Exec(query, url)
	fmt.Println(result)
	if err != nil {
		return err
	}

	return nil
}

func firstMenu(sc *bufio.Scanner, count int) int {
	re := regexp.MustCompile(`\d+`)

	for {
		fmt.Printf("Please select an entry from 0 to %d or enter n", count+1)
		if !sc.Scan() {
			log.Fatal("Failed to get text from terminal")
		}

		txt := sc.Text()

		if txt == "n" {
			return -1
		} else if re.MatchString(txt) {
			num, err := strconv.Atoi(txt)
			if err != nil {
				log.Fatal(err)
			}
			return num
		} else {
			continue
		}
	}
}

func newURLMenu(sc *bufio.Scanner) string {
	re := regexp.MustCompile(`^https?://`)
	for {
		fmt.Println("Please enter the new url: ")
		if !sc.Scan() {
			fmt.Println("Failed to get the url from the terminal")
		}
		txt := sc.Text()
		if !re.MatchString(txt) {
			v := httpOrHttps(sc)

			if v == 1 {
				txt = "https://" + txt
			} else {
				txt = "http://" + txt
			}
		}
		return txt
	}
}

func httpOrHttps(sc *bufio.Scanner) int {
	for {
		fmt.Println("Please select 0 for http or 1 for https")
		if !sc.Scan() {
			fmt.Println("Failed to get http https option")
		}
		txt := sc.Text()

		if txt == "1" {
			return 1
		} else if txt == "0" {
			return 0
		} else {
			continue
		}

	}
}

func printBody(sc *bufio.Scanner, hr *http.Response) {
	for {
		fmt.Println("Would you like view the body of the responese as well? (Y/n): ")
		if !sc.Scan() {
			fmt.Println("Failed to check whether the user wants to view the body")
		}

		txt := sc.Text()

		txt = strings.ToLower(txt)

		if txt == "" || txt == "y" {
			body, err := io.ReadAll(hr.Body)
			if err != nil {
				fmt.Println("Failed toget the body of the request: " + err.Error())
			}

			fmt.Println("Response Body: " + string(body))
			return
		} else {
			return
		}

	}
}

func main() {
	db, err := initDB("./urls.db")

	if err != nil {
		log.Fatal(err)
	}

	sc := bufio.NewScanner(os.Stdin)

	fmt.Println("Welcome to the HTTP Response Viewer! ")

	count, err := listEntries(db)

	if err != nil {
		log.Fatal("Could not list entries: " + err.Error())
	}

	num := firstMenu(sc, count)

	url := ""

	if num >= 0 {
		u, err := getURLById(db, num)
		if err != nil {
			log.Fatal("Failed to get entry from database: " + err.Error())
		}
		url = u
	} else {
		newUrl := newURLMenu(sc)
		err := addURL(db, newUrl)

		if err != nil {
			log.Fatal("Failed to add the new url to database" + err.Error())
		}
		url = newUrl
	}

	response, err := http.Get(url)

	if err != nil {
		fmt.Println("Error Fetching URL:", err)
	}
	defer response.Body.Close()

	fmt.Println("Status Code: ", response.StatusCode)

	fmt.Println("Headers:")
	for key, value := range response.Header {
		fmt.Println(key+":", value)
	}

	printBody(sc, response)

}
