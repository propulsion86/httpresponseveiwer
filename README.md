# HTTPResponseVeiwer

## Why did I created this.

My mother is a software tester that uses postman to test APIs. I was interested in how postman collected the headers of requests and responses and the bodies of both. So I decided to try and implement a simplified version in go.

## What does the program do?

This program allows you to give it a url (either http://www.youtube.com or www.youtube.com) and recieve the information that is in the response header. It then gives you the option to printout the body that the response contains.

## Notes

This project is not meant for use as a development tool. Since I created it to learn about http and databases in go, it has issues such as it doesn't like it when you give it an https url. These will fixed in the future.

## Future plans

I plan to fix the issues regarding urls and also I may implement a way to create your own http request headers.
